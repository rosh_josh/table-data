export const nextPage = (pageNo, data) => {
  return {
    type: "NEXT_PAGE",
    payload: pageNo,
    data : data
  };
};
export const previousPage = (pageNo, data) => {
  return {
    type: "PREVIOUS_PAGE",
    payload: pageNo,
    data : data
  };
};