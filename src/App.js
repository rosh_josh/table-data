import React, { Component } from 'react';
import "./App.css"
class App extends Component {

  constructor(props){
      super(props);
      this.state={
          posts:[],
          start : 0,
          page: 1
      }
  }
  
  componentDidMount() {
    const { start } = this.state;
    const url = `http://jsonplaceholder.typicode.com/photos?_start=` + this.state.start + `&_limit=5`;
    fetch(url, {
      method: 'GET',
    }).then(response => response.json()).then(posts => {
      this.setState({ posts: [...posts], start: start + 5 });
    })
  }
  handleNext = () =>{
    this.setState({ start : this.state.start + 5});
    this.setState({ page : this.state.page + 1});
    const url = `http://jsonplaceholder.typicode.com/photos?_start=`+this.state.start+`&_limit=5`;
    fetch(url, {
      method: 'GET',
    }).then(response => response.json()).then(posts => {
     this.setState({ posts : [...posts]}); 
    })

  }
  handlePrevious = () =>{
    this.setState({ start : this.state.start - 5});
    this.setState({ page : this.state.page - 1});
    const url = `http://jsonplaceholder.typicode.com/photos?_start=`+this.state.start+`&_limit=5`;
 
    fetch(url, {
      method: 'GET',
    }).then(response => response.json()).then(posts => {
     this.setState({ posts : [...posts]}); 
     console.log(posts)
    })
  }
   handleDelete =( id)=>{
    console.log(id);
    const arr = this.state.posts.filter(c => c.id!== id);
    this.setState({posts : arr});
   }
  
  render() {
    
    return (
     <React.Fragment>
      <div className="table__wrapper">
        <table>
          <thead>
            <tr>
              <th></th>
              <th>Album</th>
              <th>Title</th>
              <th>URL</th>
              <th>Action</th>
            </tr> 
          </thead>
          <tbody>
            {this.state.posts.map(item => 
              <tr key={item.id}>
                <td><img src={item.thumbnailUrl} /></td>
                <td>{item.albumId}</td>
                <td>{item.title}</td>
                <td><a href={item.url} target="_blank">{item.url}</a></td>
                <td><button className="del__button" onClick={() =>this.handleDelete(item.id)}>Delete</button></td>
              </tr>
            )}
          </tbody>
        </table>
       </div>
        <div className="detail__section">
          <p>Page {this.state.page}</p>
        </div>
       <div className="button__container">
        <button className={this.disablePrevious()} onClick = {this.handlePrevious}>Previous</button>
        <button onClick = {this.handleNext}>Next</button>
       </div>
     </React.Fragment>

    );
  }

  disablePrevious(){
    let classes = "btn ";
    classes += this.state.page ===1 ? "disabled" : "enabled";
    return classes
  }
}

export default App;