import  paginationReducer from "./pagination";
import { combineReducers } from "redux";

const allReducers = combineReducers({ paginationReducer });

export default allReducers;
