const paginationReducer = (start = 5, action) => {
    switch(action.type){
        case "previous" :
            return start - 5
        case "next" :
            return start + 5
        default :
            return start;
    }
}
export default paginationReducer;